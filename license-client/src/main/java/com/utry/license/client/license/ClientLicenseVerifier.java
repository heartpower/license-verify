package com.utry.license.client.license;

import com.utry.license.common.ExtraLicenseContent;
import com.utry.license.common.verify.CustomLicenseVerifier;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

import java.util.Map;

/**
 *
 * @description 自定义校验逻辑
 *
 * @author renxin
 * @email renxinzhiliangzhi@163.com
 * @date 2019/7/30 13:28
 */
@Slf4j
@Component
public class ClientLicenseVerifier implements CustomLicenseVerifier {

    @Override
    public Map<String, Map<String, String>> customVerify(ExtraLicenseContent content) {

        log.info("自定义校验逻辑执行！");
        return content.getCustomContent();
    }
}

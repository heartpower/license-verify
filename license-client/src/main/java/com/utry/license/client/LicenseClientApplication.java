package com.utry.license.client;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;

/**
 *
 * @description 启动类
 *
 * @author renxin
 * @email renxinzhiliangzhi@163.com
 * @date 2019/7/25 17:25
 */
@ComponentScan(basePackages = {"com.utry.license.common.verify","com.utry.license.client"})  // 引入权限校验模块包扫描
@SpringBootApplication
public class LicenseClientApplication {

    public static void main(String[] args) {
        SpringApplication.run(LicenseClientApplication.class, args);
    }

}

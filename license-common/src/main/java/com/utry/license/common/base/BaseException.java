package com.utry.license.common.base;

import lombok.Data;

/**
 *
 * @description 基础异常定义
 *
 * @author renxin
 *
 * @email renxinzhiliangzhi@163.com
 * 
 * @date 2019/7/25 14:34
 */
@Data
public class BaseException extends RuntimeException{

    private Integer code;

    private String message;

    public BaseException(ErrorEnum errorEnum) {

        super(errorEnum.getMessage());
        this.code = errorEnum.getCode();
        this.message = errorEnum.getMessage();
    }

    public BaseException(Integer code, String message) {
        super(message);
        this.code = code;
        this.message = message;
    }

    public BaseException(String message) {
        super(message);
    }
}

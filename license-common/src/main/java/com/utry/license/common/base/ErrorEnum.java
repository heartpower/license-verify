package com.utry.license.common.base;

import lombok.Getter;

/**
 *
 * @description 全局错误码
 *
 * @author renxin
 *
 * @email renxinzhiliangzhi@163.com
 * 
 * @date 2019/7/25 14:39
 */
@Getter
public enum ErrorEnum {

    /**  成功 */
    SUCCESS(0,"成功"),
    /**  配置错误 */
    CONFIGURATION_ERROR(1000,"配置错误"),
    /**  日期解析错误 */
    DATE_PARSE_ERROR(2001,"日期解析异常"),
    /** 证书生成错误*/
    LICENSE_CREATE_ERROR(3001,"证书生成错误"),
    /**  证书安装失败 */
    LICENSE_INSTALL_ERROR(3002,"证书安装失败"),
    /**  证书校验不通过 */
    LICENSE_VERIFY_FAIL(3003,"证书校验失败"),
    /**  证书文件下载失败 */
    LICENSE_DOWNLOAD_ERROR(3004,"证书文件下载失败"),
    /**  证书文件超过上限 */
    LICENSE_TOO_LONG(3005,"证书文件大小超过上限"),
    /**  参数异常 */
    PARAMETER_ILLEGAL(4000,"参数校验异常"),
    /**  系统异常 */
    SYSTEM_ERROR(9000,"系统异常"),
    ;

    private Integer code;

    private String message;

    ErrorEnum(Integer code, String message) {
        this.code = code;
        this.message = message;
    }
}

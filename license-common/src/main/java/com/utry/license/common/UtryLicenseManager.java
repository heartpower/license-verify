package com.utry.license.common;

import de.schlichtherle.license.*;
import de.schlichtherle.xml.GenericCertificate;

/**
 *
 * @description 扩展原有证书管理器功能，输出为字节数组，方便下载
 *
 * @author renxin
 *
 * @email renxinzhiliangzhi@163.com
 * 
 * @date 2019/7/26 10:20
 */
public class UtryLicenseManager extends LicenseManager {


    public UtryLicenseManager(LicenseParam param) {
        super(param);
    }

    /**
     * 这里扩展下载证书方法
     *
     * @param licenseContent
     * @return
     * @throws Exception
     */
    public byte[] getLicenseBytes(LicenseContent licenseContent) throws Exception{

        return create(licenseContent);

    }

    /**
     * 原有verify方法证书内容加载30分钟内不再校验，这里新增实时的verify方法
     *
     * {@link LicenseManager#verify(de.schlichtherle.license.LicenseNotary)}
     *
     * @return
     * @throws Exception
     */
    public synchronized LicenseContent cachelessVerify() throws Exception{

        LicenseNotary notary = getLicenseNotary();

        // Load license key from preferences,
        final byte[] key = getLicenseKey();
        if (null == key) {
            throw new NoLicenseInstalledException(getLicenseParam().getSubject());
        }
        GenericCertificate certificate = getPrivacyGuard().key2cert(key);
        notary.verify(certificate);
        final LicenseContent content = (LicenseContent) certificate.getContent();
        validate(content);
        setCertificate(certificate);

        return content;

    }
    
}

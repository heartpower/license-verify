package com.utry.license.common;

import de.schlichtherle.license.LicenseContent;
import de.schlichtherle.license.LicenseManager;
import de.schlichtherle.license.LicenseParam;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

import java.util.Objects;

/**
 *
 * @description 保证证书管理器的单例，包含两种证书管理器
 *
 * <p>
 *     <li>基础证书管理器</li>
 *     <li>扩展证书管理器</li>
 * </p>
 *
 * @author renxin
 *
 * @email renxinzhiliangzhi@163.com
 * 
 * @date 2019/7/25 17:16
 */
@Slf4j
public class LicenseManagerHolder {

    /**  扩展证书管理器 */
    private static UtryLicenseManager utryLicenseManager;

    /**
     * 获取全局唯一的扩展证书管理器
     *
     * @param licenseParam
     * @return
     */
    public static synchronized UtryLicenseManager getUtryLicenseManager(LicenseParam licenseParam){

        return utryLicenseManager != null ? utryLicenseManager : new UtryLicenseManager(licenseParam);

    }

}

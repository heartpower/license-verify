package com.utry.license.common.base;
/**
 *
 * @description 异常工具类
 *
 * @author renxin
 *
 * @email renxinzhiliangzhi@163.com
 * 
 * @date 2019/7/25 14:51
 */
public class ErrorUtil {

    private ErrorUtil() {}

    public static BaseException buildBaseException(Integer code,String message){

        return new BaseException(code,message);
    }

    public static BaseException buildBaseException(ErrorEnum errorEnum){

        return new BaseException(errorEnum);
    }

}

package com.utry.license.common.verify;

import lombok.Data;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;

/**
 *
 * @description 证书校验参数,公钥库和密码
 *
 * @author renxin
 *
 * @email renxinzhiliangzhi@163.com
 * 
 * @date 2019/7/25 17:07
 */
@Slf4j
@Data
@Configuration
@PropertySource(value = "classpath:license/config.properties",encoding = "utf-8")
@ConfigurationProperties(prefix = "utry")
public class LicenseConfigBean {

    /**  证书签名 */
    private String subject;
    /**  公钥别名 */
    private String publicAlias;
    /**  密钥库密码 */
    private String storePwd;
    /**  证书文件所在位置 */
    private String licensePath;
    /**  公钥库所在位置 */
    private String publicStorePath;

}

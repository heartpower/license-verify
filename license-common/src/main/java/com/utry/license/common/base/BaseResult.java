package com.utry.license.common.base;

import lombok.Data;

/**
 *
 * @description 全局接口响应结果
 *
 * @author renxin
 *
 * @email renxinzhiliangzhi@163.com
 * 
 * @date 2019/7/25 15:22
 */
@Data
public class BaseResult<T> {

    private Integer code;

    private String message;

    private T data;

    private BaseResult(){}

    public static  <R>BaseResult<R> build(R data){

        BaseResult<R> result = build();
        result.setData(data);
        return result;

    }

    public static BaseResult build(){

        BaseResult result =  new BaseResult();
        result.setCode(ErrorEnum.SUCCESS.getCode());
        result.setMessage(ErrorEnum.SUCCESS.getMessage());

        return result;
    }

    public static BaseResult build (ErrorEnum errorEnum){

        BaseResult result =  new BaseResult();
        result.setCode(errorEnum.getCode());
        result.setMessage(errorEnum.getMessage());

        return result;
    }

    public static BaseResult build(BaseException e){
        BaseResult result =  new BaseResult();
        result.setCode(e.getCode());
        result.setMessage(e.getMessage());
        return result;
    }

    public static BaseResult build(Integer code,String message){
        BaseResult result =  new BaseResult();
        result.setCode(code);
        result.setMessage(message);
        return result;
    }


}

package com.utry.license.common.verify;

import com.utry.license.common.ExtraLicenseContent;

import java.util.Map;

/**
 *
 * @description 证书扩展功能校验接口
 *
 * @author renxin
 * @email renxinzhiliangzhi@163.com
 * @date 2019/7/30 13:13
 */
public interface CustomLicenseVerifier {

    /**
     * 扩展校验
     *
     * @param content
     * @return
     */
    Map<String, Map<String, String>> customVerify(ExtraLicenseContent content);

}

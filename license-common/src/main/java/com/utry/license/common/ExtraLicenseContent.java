package com.utry.license.common;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Map;
import java.util.Set;

/**
 *
 * @description 证书内容扩展参数
 *
 * @author renxin
 * @email renxinzhiliangzhi@163.com
 * @date 2019/7/29 13:16
 */
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class ExtraLicenseContent {

    /**  子站编码 */
    private Set<String> subSiteCodeSet;
    /**  允许的mac地址 */
    private Set<String> macSet;
    /**  允许的ip地址 */
    private Set<String> ipAddressSet;
    /**  自定义扩展校验参数 */
    private Map<String, Map<String,String>> customContent;

}

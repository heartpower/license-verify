package com.utry.license.common.verify;

import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

import java.net.InetAddress;
import java.net.NetworkInterface;
import java.net.SocketException;
import java.util.*;

/**
 *
 * @description 证书扩展参数获取，这里抽象，针对不同操作系统做不同实现
 *
 * @author renxin
 * @email renxinzhiliangzhi@163.com
 * @date 2019/7/29 15:26
 */
@Component
@Slf4j
public class LicenseExtraContentHunter {

    /**
     * 获取机器所有ip地址
     *
     * @return
     * @throws Exception
     */
    protected static Set<String> getIpAddress() throws Exception{

        HashSet ipSet = new HashSet<>(16);

        //获取所有网络接口
        List<InetAddress> inetAddresses = getLocalAllInetAddress();

        if(inetAddresses != null && !inetAddresses.isEmpty()){
            for(InetAddress inetAddr : inetAddresses){

                ipSet.add(inetAddr.getHostAddress().toLowerCase());

            }
            return ipSet;
        }

        return Collections.EMPTY_SET;
    }

    /**
     * 获取机器所有mac地址
     *
     * @return
     * @throws Exception
     */
    protected static Set<String> getMacAddresses() throws Exception{

        HashSet macSet = new HashSet<>(16);
        List<InetAddress> inetAddresses = getLocalAllInetAddress();
        if(inetAddresses != null && !inetAddresses.isEmpty()){

            for(InetAddress inetAddr : inetAddresses){

                macSet.add(getMacByInetAddress(inetAddr));
            }

            return macSet;
        }
        return Collections.EMPTY_SET;
    }

    /**
     * 获取所有IPADDRESS
     *
     * @return
     * @throws Exception
     */
    protected static List<InetAddress> getLocalAllInetAddress() throws Exception {

        List<InetAddress> result = new ArrayList<>(4);
        Enumeration<NetworkInterface> interfaceEnumeration = NetworkInterface.getNetworkInterfaces();
        while(interfaceEnumeration.hasMoreElements()){
            // 取出网卡接口
            NetworkInterface iface = interfaceEnumeration.nextElement();
            // 取出网卡接口下的IP
            Enumeration<InetAddress> inetAddressEnumeration = iface.getInetAddresses();
            while(inetAddressEnumeration.hasMoreElements()){
                InetAddress inetAddr = inetAddressEnumeration.nextElement();

                //排除LoopbackAddress、SiteLocalAddress、LinkLocalAddress、MulticastAddress类型的IP地址
                if(!inetAddr.isLoopbackAddress()
                        && !inetAddr.isLinkLocalAddress()
                        && !inetAddr.isMulticastAddress()){
                    result.add(inetAddr);
                }
            }
        }

        return result;
    }

    /**
     * 获取mac
     *
     * @param inetAddr
     * @return
     */
    protected static String getMacByInetAddress(InetAddress inetAddr) throws SocketException{

        byte[] mac = NetworkInterface.getByInetAddress(inetAddr).getHardwareAddress();
            StringBuffer stringBuffer = new StringBuffer();

            for(int i=0;i<mac.length;i++){
                if(i != 0) {
                    stringBuffer.append("-");
                }

                //将十六进制byte转化为字符串
                String temp = Integer.toHexString(mac[i] & 0xff);
                if(temp.length() == 1){
                    stringBuffer.append("0" + temp);
                }else{
                    stringBuffer.append(temp);
                }
            }

            return stringBuffer.toString().toUpperCase();

    }

}

package com.utry.license.server.dto;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.utry.license.common.ExtraLicenseContent;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import org.hibernate.validator.constraints.NotBlank;
import org.hibernate.validator.constraints.NotEmpty;
import org.springframework.beans.BeanUtils;
import org.springframework.format.annotation.DateTimeFormat;

import javax.validation.constraints.NotNull;
import java.util.Date;
import java.util.Map;
import java.util.Set;

/**
 *
 * @description 证书生成入参
 *
 * @author renxin
 * @email renxinzhiliangzhi@163.com
 * @date 2019/7/26 14:30
 */
@Data
@ApiModel(value = "证书创建参数",description = "证书创建扩展参数")
public class LicenseCreateDTO {

    /** 证书过期时间 */
    @NotNull(message = "过期时间不能为空")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss",timezone="GMT+8" )
    @ApiModelProperty("证书有效期截止时间：格式yyyy-MM-dd HH:mm:ss")
    private Date expireTime;
    /**  子站编码 */
    @NotEmpty(message = "子站编码不能为空")
    @ApiModelProperty("允许的子站编码")
    private Set<String> subSiteCodeSet;
    /**  允许的mac地址 */
    @NotEmpty(message = "mac地址不能为空")
    @ApiModelProperty("允许允许机器的mac地址")
    private Set<String> macSet;
    /**  允许的ip地址 */
    @ApiModelProperty("允许允许机器的IP地址")
    @NotEmpty(message = "IP地址不能为空")
    private Set<String> ipAddressSet;
    /**  自定义扩展校验参数 */
    @ApiModelProperty("自定义扩展参数Map<String, Map<String,String>>")
    private Map<String, Map<String,String>> customContent;

    public ExtraLicenseContent convert2ExtraLicenseContent(){

        ExtraLicenseContent content = new ExtraLicenseContent();
        BeanUtils.copyProperties(this,content);
        return content;
    }

    public LicenseCreateDTO(Date expireTime) {
        this.expireTime = expireTime;
    }
}

package com.utry.license.server;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 *
 * @description 启动类
 *
 * @author renxin
 *
 * @email renxinzhiliangzhi@163.com
 *
 * @date 2019/7/25 17:25
 */
@SpringBootApplication
public class LicenseServerApplication {

    public static void main(String[] args) {
        SpringApplication.run(LicenseServerApplication.class, args);
    }

}

package com.utry.license.server.license;

import lombok.Data;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;

/**
 *
 * @description 
 *
 * @author renxin
 *
 * @email renxinzhiliangzhi@163.com
 * 
 * @date 2019/7/25 11:29
 */
@Slf4j
@Data
@Configuration
@PropertySource(value = "classpath:license/config.properties",encoding = "utf-8")
@ConfigurationProperties(prefix = "utry")
public class LicenseConfigBean {

    private String licenseFileName;

    private String consumerType;

    private Integer consumerAmount;

    private String info;

    private String privateAlias;

    private String keyPwd;

    private String storePwd;

    private String subject;

    private String privateStorePath;

}
